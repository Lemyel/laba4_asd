#include<vector>
#include<algorithm>
#include<iostream>
#include<fstream>
using namespace std;

///������ ������ - ����� 1..n
// ������ ������ i
class Vertex {
public:
	const int id;
	Vertex(int id) : id(id)
	{

	}
};

// �����
class Edge {
public:
	const Vertex &from, &to;
	int w;// ��� �����	
	bool visit = false;
	Edge(Vertex& v1, Vertex& v2, int w) :
		from(v1), to(v2), w(w)
	{

	}
	Edge(Vertex& v1, Vertex& v2) :
		from(v1), to(v2), w(1)
	{

	}
};



class Graph {
public:
	//������ ������
	std::vector<Vertex> nodes;
	// ������ �������� ���������� �� �����, esFrom[5] - ����� �� ������� 5, 
	// � ��� from.id = 5, to=���� �����, w=��� �����
	//������� ����
	std::vector< std::vector<Edge*> > esFrom;
	//������� ��������� ������
	std::vector< std::vector<Edge*> > min_esFrom;
	std::vector< std::vector<Edge*> > inf;
	// ������ ���������� ������
	vector<int> true_nodes;
	vector<bool> visited_nodes;
	//������ �����
	std::vector<Edge*> edges;
	std::wofstream f;

	Graph(int n)
	{
		for (int i = 0; i < n; i++)
		{
			nodes.push_back(Vertex(i));
		}
		esFrom.resize(n);
		visited_nodes.resize(n, false);
		min_esFrom.resize(n);
		inf.resize(n);
		for (int i = 0; i < esFrom.size(); i++)
		{
			Edge* a = new Edge(nodes[0], nodes[0], 101);
			inf[i].push_back(a);
			for (int j = 0; j < esFrom.size(); j++) 
			{
				Edge* e = new Edge(nodes[i], nodes[j], 101);
				esFrom[i].push_back(e);
				min_esFrom[i].push_back(e);
			}
		}
	}
	
	void AddEdge(int from, int to) 
	{
		if (from == to) return;
		Edge* e = new Edge(nodes[from], nodes[to], 1);
		edges.push_back(e);
		esFrom[from].push_back(e);
		esFrom[to].push_back(e);
	}
	void AddEdge(int from, int to, int w)
	{
		if (from == to) return;
		Edge* e = new Edge(nodes[from], nodes[to], w);
		Edge* a = new Edge(nodes[to], nodes[from], w);
		edges.push_back(e);
		esFrom[from][to] = e;
		esFrom[to][from] = e;
	}

	// ����������� ����� � ������
	Edge* min_element_in_line(vector<Edge*> &line)
	{
		Edge* min_elem;
		min_elem = inf[0][0];
		for (int i = 0; i < line.size(); i++)
			if ((min_elem->w >= line[i]->w)&&(!visited_nodes[i]))
				min_elem = line[i];
		return min_elem;
	}

	// ���� ����������� ����� � ���������(�������) ���
	Edge* MinEdge(vector<vector<Edge*>> matrix)
	{
		Edge* minEdge;
		vector<Edge*> minEdges;
		int count = 0;

		for (int i : true_nodes)
		{
				minEdges.push_back(min_element_in_line(matrix[i]));
		}

		minEdge = *min_element(minEdges.begin(), minEdges.end(),
			[](Edge* lhs, Edge* rhs) {return lhs->w < rhs->w; });
		//�������� ���������� �������
		if (!visited_nodes[minEdge->from.id])
		{
			true_nodes.push_back(minEdge->from.id);
			visited_nodes[minEdge->from.id] = true;
		}
		if (!visited_nodes[minEdge->to.id])
		{
			true_nodes.push_back(minEdge->to.id);
			visited_nodes[minEdge->to.id] = true;
		}

		cout << "(" << minEdge->from.id << "," << minEdge->to.id << ")";
		cout << minEdge->w << " ";
		return minEdge;
	}

	// �������� �����
	void Algorithm()
	{
		
		for (int i = 0; i < esFrom.size(); i++)
		{
			for (int j = 0; j < esFrom[i].size(); j++)
			{
				//cout << "(" << esFrom[i][j]->from.id << "," << esFrom[i][j]->to.id << ")";
				cout << esFrom[i][j]->w << "\t";
			}
			cout << endl;
		}
		cout << endl << "===================================================== " << endl;

		f << "pause\n";
		for (auto& n : nodes) 
		{
			f << n.id << std::endl;
		}
		true_nodes.push_back(0);
		visited_nodes[0] = true;
		//���� ����������� ����� ��� ����������� ��������� ������
		for (int i = 0; i < esFrom.size() - 1; i++)
		{
			PrintAlgorithm(MinEdge(esFrom));
		}
		f << "draw\n";
		cout << endl << "===================================================== " << endl
			<< "Spanning tree matrix" << endl;
		for (int i = 0; i < min_esFrom.size(); i++)
		{
			for (int j = 0; j < min_esFrom[i].size(); j++)
			{
				cout << min_esFrom[i][j]->w << "\t";
			}
			cout << endl;
		}		
		
	}

	void PrintAlgorithm(Edge* e)
	{
		f << e->from.id << ",color=red" << endl;
		if (e->from.id <= e->to.id)
			f << e->from.id << "-" << e->to.id << ",label=" << e->w << ",color=red\n";
		else
			f << e->to.id << "-" << e->from.id << ",label=" << e->w << ",color=red\n";
		f << "w,600\n";
		f << e->to.id << ",color=red" << endl;	
		f << "p,("<< e->from.id <<", "<< e->to.id << ")w="<< e->w<< endl;
	}

	void WriteGraph(std::string fileName) {
		f = std::wofstream(fileName);
		f << "window.prog=`\n";
		for (auto &n : nodes) {
			f << n.id << std::endl;
		}
		for (auto e : edges) {
			WriteEdge(e->from.id, e->to.id, e->w); 
			f << std::endl;
		}

		f << "draw\n";
	}

	void WriteEdge(int i, int j, int w)
	{
		if (i <= j) 
			f << i << "-" << j <<",label="<<w;
		else 
			f << j << "-" << i << ",label=" << w ;
	}

	void WriteEnd() {
		f << "`\n";
		f.close();
	}

	~Graph() {
		for (size_t i = 0; i < edges.size(); i++)
		{
			delete edges[i];
		}
	}
};