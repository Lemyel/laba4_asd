#include "Graph.h"

int main() {
	{
		Graph g(7);
		/*g.AddEdge(0, 1, 4);
		g.AddEdge(0, 2, 2);
		g.AddEdge(0, 3, 1);
		g.AddEdge(1, 3, 9);
		g.AddEdge(1, 4, 6);
		g.AddEdge(1, 6, 8);
		g.AddEdge(2, 3, 6);
		g.AddEdge(2, 5, 5);
		g.AddEdge(3, 4, 5);
		g.AddEdge(3, 5, 6);
		g.AddEdge(3, 6, 7);
		g.AddEdge(4, 6, 4);
		g.AddEdge(5, 6, 8);*/

		//Graph g(7);
		g.AddEdge(0, 1, 7);//A
		g.AddEdge(0, 3, 5);

		g.AddEdge(1, 2, 8);//B
		g.AddEdge(1, 3, 9);
		g.AddEdge(1, 4, 7);


		g.AddEdge(2, 4, 5);//C

		g.AddEdge(3, 4, 15);//D
		g.AddEdge(3, 5, 6);

		g.AddEdge(4, 5, 8);//E
		g.AddEdge(4, 6, 9);

		g.AddEdge(5, 6, 11);//F



		g.WriteGraph("../../../vis/commands.js");
		g.Algorithm();
		g.WriteEnd();
		
	}
	return 0;
}